# -*- coding: utf-8 -*-
VERSION = '1.4'
## Import external modules
import numpy as np
import time
import matplotlib.pyplot as plt

## Fix
import os
#os.chdir('c:\\users\\Neken\\Downloads\\compression')

## Import internal modules
from module_pixels import *
from module_image_jacquin import * 
from module_io import * 
from module_compression import *
from module_evaluation import *

## Import extensions
from extension_rgb import *

banque = os.listdir('banque')

## Images url config
target_url = 'banque/geba_river_guinea-bissau_512x512.bmp'
target_convert_to_grey = 1
attractor_url = ''
attractor_convert_to_grey = 0

## Misc config
N = 30

## Credit
print('Fractal compression, version '+str(VERSION))
print('All credit goes to S. RAZAFINDRABE and M. ORHAN')

## Chargement de target
target = load_image(target_url)
height, width = np.shape(target)[0], np.shape(target)[1]

## Chargement de attractor
if attractor_url=='':
    attractor = gen_image(height, width)
    attractor_url = 'Random pixels'
else:
    attractor = load_image(attractor_url)


## Eventuelle conversion vers le gris
if target_convert_to_grey:
    target = to_grey(target)
if attractor_convert_to_grey:
    attractor = to_grey(attractor)
    
## Detection RGB
if in_greyscale(target):
    RGB = 0
    print('Greyscale detected.')
else:
    RGB = 1
    print('RGB detected.')

###Evaluation et sauvegarde
evaluate = True
save = True #Si pas de sauvegarde, savename = False
savename = 'river'

## Rapport des paramètres à l'utilisateur (à completer)
print('********')
print('Session:')
print('Target : '+target_url+', '+str(width)+'x'+str(height))
print('Attractor : '+attractor_url)
print('********')

## Bloc size config
#pour comparer
sizes = [(32,16), (16, 8), (8,4), (4,2)]
#sizes = [(64,32),(32,16),(16,8),(8,4),(4,2)]
seuil = 255^2
source_size = 64 # Taille bloc source
destination_size = 32 # Taille bloc destination

## Debut de la mesure de temps
start = time.clock()

## Compression & Decompression

if evaluate or save:
    config = target, attractor, RGB, N, sizes, evaluate, save, savename
    result = apply(target, attractor, RGB, N, sizes, evaluate, save, savename)
    #result = image_decompression, errors, compression_times, decompression_times
    
else:
    if RGB:
        print('Compression (RGB)...')
        ifs = compress_rgb(target, source_size, destination_size)
        print('Done.')
        print('Decompression (RGB)...')
        result = decompress_rgb(source_size, destination_size, ifs, [height, width], N)
        print('Done.')
        show_image_rgb(result)
    else:
        print('Compression (greyscale)...')
        ifs = compress(target,source_size, destination_size)
        print('Done.')
        print('Decompression (greyscale)...')
        result = decompress(source_size, destination_size, ifs, attractor,N) 
        print('Done.')
        show_image(result)


## Fin de la mesure de temps
end = time.clock()

print(str(end - start) + 's')