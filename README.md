# Fractal Compression

## Launch
To launch the script:

* Open the main.py file in an IDE such as Spyder 
* Run the script
* The output which corresponds to the decompression iterations is saved in the folder "savename + size_grid_1 x size_grid_2", 

## Parameters
The main.py file contains the main script to execute the code. The following lines correspond to the parameters of the compression:

* target_url: path to the image to compress. An image bank inside the folder "banque" has been added for testing purposes
* target_convert_to_grey: 1 if the compression is done in grayscale, 0 otherwise
* attractor_url: path to the initial image for decompression. If none is given, a randomly generated image is used. 
* attractor_convert_to_grey: 1 if the decompression is done in greyscale, 0 otherwise
* evaluate: True if the compression/decompression loss and distortion are evaluated, False otherwise
* save: True if the image iterations should be saved, False otherwise
* savename: name of the folder to save the images
* sizes: Sizes of the grid in pixels. Noted as a list (size_grid_1, size_grid_2)
* seuil: threshold associated with the MSE 
* N: number of iterations for decompression

