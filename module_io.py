# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import scipy.misc as scm
import struct
from PIL import Image
import pickle
import os

##Contient toutes les fonctions liées aux entrées et sorties,
##comme le chargement d'une image et sa sauvegarde.

def load_image(image_name):
    """
    Parameters: image_name = nom du fichier sur l'ordi en str
    Return: image sous la forme numpy.array"""
    print('Loading picture...')
    image = scm.imread(image_name)
    return np.array(image, dtype = 'uint8')

def save_image(np_image, image_name):
    """
    Parameters:     
    np_image = image sous la forme d'un np.array (matrice)
    image_name = nom de l'image"""
    scm.imsave(image_name, np_image)

def gen_image(height, width):
    return np.random.randint(255, size = (height, width))
    
def show_image(np_image):
    plt.imshow(np_image, cmap = 'gray')#cmap=palette de couleurs (niveau de gris) gray ou Greys_r
    plt.show(block=False)

def to_folder(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)

def save_iteration(partitionnement, save_name, image, i):
    """i = numéro de l'itération"""
    to_folder('Resultats\\' + save_name)
    to_folder('Resultats\\' + save_name + '\\' + str(partitionnement[0]) + str(partitionnement[1]))
    filename = 'Resultats\\' + save_name + '\\' + str(partitionnement[0]) + str(partitionnement[1]) + '\\iteration' + str(i) + '.jpg'
    scm.imsave(filename, image)

def load_result(filename):
    file = open(filename, 'rb')
    result = pickle.load(file)
    file.close()
    return result
    
def save_result(object, filename):
    """object = instance python"""
    file = open(filename, 'wb')
    pickle.dump(object, file)
    file.close()
    
def to_grey(image):
    # norme iso
    print('Converting RGB to grey scale...')
    return np.array([[0.2125*pix[0] + 0.7154*pix[1] + 0.0721*pix[2] for pix in line] for line in image])
    
def around(pixels):
    def vect(pixel):
        if pixel > 255:
            return 255
        elif pixel < 0:
            return 0
        return pixel
        
    transform = np.vectorize(vect)
    data = transform(pixels)
    return np.round(data).astype('uint8')
###### NOT USED OR DEPRECIATED ########

def pgcd(a,b):
    while (b>0):
        r=a%b
        a,b=b,r
    return a

def crop(np_image, size_bloc1, size_bloc2):
    #change la taille de l'image pour éviter les problèmes de bordures
    (n,m)=np.shape(np_image)
    M = int(size_bloc1*size_bloc2/pgcd(size_bloc1, size_bloc2))
    size = (n-n%M, m-m%M)
    return scm.imresize(np_image, size) #size =tuple

    
def load_frc(filename):
    """
    Import sous format .frc
    En tête : largeur_image (int), hauteur_image
    
    """
    f = open(filename, "rb")
    h = struct.Struct("iiii") # En-tete (largeur, longeur, taille bloc source, taille bloc arrive)
    s = struct.Struct("iiff") # Structure (bloc source, isometrie, contraste, luminosite)
    data = []
 
    try:
        record = f.read(4)
        header = h.unpack(record)
        while True:
            record = f.read(4)
            if len(record) != 4:
                break;
            else:
                data.append(list(s.unpack(record)))
    except IOError:
        print("Erreur pendant le chargement")
        
    finally:
        f.close()
    
    return header, data
        
    
    
def save_compression(data,header,filename):
    
    h = struct.Struct("iiii") # En-tete (largeur, longeur, taille bloc source, taille bloc arrive)
    s = struct.Struct("iiff") # Structure (bloc source, isometrie, contraste, luminosite)
    
    f = open(filename, "wb")
    
    try:
        h.pack(header)
        for i in range(len(data)):
            s.pack(data[i])
    
    except IOError:
        print("Erreur pendant la sauvegarde")
        
    finally:
        f.close()
        



    
