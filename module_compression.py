# -*- coding: utf-8 -*-

## Pour ce fichier les opérations de stockage, etc sont
## transparentes
import numpy as np
import scipy.misc as scm
from module_pixels import *
from module_io import * 
from module_bloc_jacquin import * 
from module_image_jacquin import * 
from module_evaluation import *

def compress(image,source_size, destination_size,verbose=True):
    
    sources = ImageJacquin(image, source_size) # partitionnement source
    destinations = ImageJacquin(image, destination_size) # partitionnement destination
    n=len(destinations.blocs)
    ifs = []
    
    for i in range(len(destinations.blocs)):
        
        bloc_d = destinations.get_bloc(i) 
        error_min = float('inf')
        
        for j in range(len(sources.blocs)):
            
            bloc_s = sources.get_bloc(j)
            bloc_s.to_float()
            
            for k in range(len(bloc_s.transformations)):
                
                
                transformed = bloc_s.transformations[k]() # Appliquer la transformation k au bloc source
                transformed = transformed.reduce(source_size,destination_size)
                C,L = transformed.regression_lin(bloc_d) # Regression linéaire entre bloc résultant et bloc destination
                C = round(C,2) #quantifie
                L  = int(L) #quantifie
                error = bloc_d.distance(transformed.contrast_lux(C,L))
                if error < error_min: # Sans condition
                    error_min = error
                    bloc_min = j
                    trf_min = k
                    C_min = C
                    L_min = L
        if verbose:
            print(str(i+1)+'/'+str(n)) # compteur
        ifs.append([bloc_min, trf_min, C_min, L_min])
    return ifs

def decompress(source_size,destination_size, ifs, image,N, save = False, evaluate = False, *reference):
    
    test = evaluation(evaluate)
    
    for k in range(N):
        
        sources = ImageJacquin(image, source_size)
        destinations = ImageJacquin(image, destination_size)
        
        for i in range(len(ifs)):
            
            bloc_s = sources.get_bloc(ifs[i][0]) # lis le bloc source min pour un bloc destination donné
            bloc_s.to_float()
            transformed = bloc_s.transformations[ifs[i][1]]() # applique la transformation min a ce bloc
            
            transformed = transformed.contrast_lux(ifs[i][2], ifs[i][3]) # applique l'adaptation de contraste
            transformed = transformed.reduce(source_size,destination_size)
            destinations.set_bloc(transformed, i) # remplace le i ème bloc de la partition destination

        if save:
            save_iteration((source_size, destination_size), save, image, k)
        if evaluate:
            test.evaluate_iteration(image, *reference) 
        image = destinations.rebuild()
        
    if evaluate:
        return around(image), test
    else:
        return around(image)
