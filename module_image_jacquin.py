# -*- coding: utf-8 -*-
## représentera Source et Destination

import numpy as np
import scipy.misc as scm
from module_pixels import *
from module_io import * 
from module_bloc_jacquin import * 
import time 

class ImageJacquin(Pixels):

    def __init__(self,data,blocs_size):
        # plus créer self.bloc, une LISTE (et non matrice) de bloc
        # en partant du coin supérieur gauche
        # en allant vers le bas puis vers la droite
        """Parameters: 
        data = donnees d'entrees recuperees depuis io sous forme matricielle"""
        self.pixels = data
        self.n, self.m = np.shape(self.pixels)[0], np.shape(self.pixels)[1] #n = nbres de lignes, m = nbres de colonnes
        self.blocs_size = blocs_size
        self.n_lines = int(self.n/self.blocs_size)
        self.n_columns = int(self.m/self.blocs_size)
        self.blocs = self.partition() 

    def get_bloc(self,key):
        # renvoi bloc[key]
        return self.blocs[key]

    def set_bloc(self,bloc,key):
        # fixe bloc[key]
        self.blocs[key] = bloc

    def rebuild(self):
        # reconstruit l'image à partir des blocs (en tant qu'objet pixels)
        # quand : on veux faire des test, ou afficher/sauvegarder l'image
        lines = []
        for i in range(self.n_lines):
            columns = []
            for j in range(i*self.n_columns, (i+1)*self.n_columns):
                #On concatene les blocs colonnes entre eux d'une même ligne 
                #(sur une même ligne, il y a n_blocs_columns, indiciés entre
                #i*n_blocs_columns et (i+1)*n_blocs columns avec i le numéro de la ligne
                #pour former une ligne reconstruite
                columns.append(self.blocs[j].pixels)
                line = np.concatenate(columns, axis = 1)
            #On concatene ensuite les lignes entre elle
            lines.append(line)
        built = np.concatenate(lines, axis = 0)
        return built
    
    def partition(self):
        """Decoupe l'image en bloc de la taille blocs_size
        Parameters: blocs_size = taille des blocs
        Return : la liste des blocs ainsi decoupees""" 
        #On decoupe la matrice en vertical, on obtient une liste de lignes
        lines = np.split(self.pixels, self.n_lines)
        blocs = []
        for line in lines:
            #Pour chaque bloc_ligne de la liste, on recoupe a nouveau en horizontal, on obtient la liste de bloc
            columns = np.hsplit(line, self.n_columns)
            for bloc in columns:
                blocs.append(BlocJacquin(bloc))
        return blocs


