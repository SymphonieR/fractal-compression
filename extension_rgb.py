# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import scipy.misc as scm
from PIL import Image

from module_pixels import *
from module_io import * 
from module_bloc_jacquin import * 
from module_image_jacquin import * 
from module_compression import *
import module_compression

def show_image_rgb(image):
    image = np.array(image,dtype='uint8')
    img = Image.fromarray(image, 'RGB')
    img.show()
    
def in_greyscale(image):
    return len(np.shape(image))==2

def separate_color(image):
    
    n, m = np.shape(image)[0], np.shape(image)[1]
    R = np.zeros(shape = (n,m))
    G = np.zeros(shape = (n,m))
    B = np.zeros(shape = (n,m))
    
    for i in range(n):
        for j in range(m):
            
            R[i,j] = image[i,j,0]
            G[i,j] = image[i,j,1]
            B[i,j] = image[i,j,2]
            
    return R, G, B
    
def superpose_color(R, G, B):
    
    n,m = np.shape(R)
    image = np.zeros(shape = (n,m,3))
    
    for i in range(n):
        for j in range(m):
            image[i][j] = [R[i][j], G[i][j], B[i][j]]
    return image
    
def compress_rgb(image, source_size, destination_size):
    R, G, B = separate_color(image)
    
    ifs_R = module_compression.compress(R, source_size, destination_size)
    ifs_G = module_compression.compress(G, source_size, destination_size)
    ifs_B = module_compression.compress(B, source_size, destination_size)
    
    return ifs_R, ifs_G, ifs_B
    
def decompress_rgb(source_size,destination_size, IFS, image, N):
    ifs_R, ifs_G, ifs_B = IFS
    
    Red = module_compression.decompress(source_size,destination_size, IFS[0], image, N)
    Green = module_compression.decompress(source_size,destination_size, IFS[1], image, N)
    Blue = module_compression.decompress(source_size,destination_size, IFS[2], image, N)
    
    
    image = superpose_color(Red, Green, Blue)
    return image
        