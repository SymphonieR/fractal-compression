# -*- coding: utf-8 -*-

import numpy as np
import scipy.misc as scm
from module_pixels import *
import scipy
from scipy import ndimage


class BlocJacquin(Pixels):

    # on verra si besoin d'un init a priori non

    # C'est là qu'on met nos transformations
    # Elles renvoient un bloc, ne modifient pas l'actuel attention !

    # self.pixels est défini
    # set_pix, get_pix,

    def __init__(self,data):
        Pixels.__init__(self,data)
        self.transformations = [self.identity, self.rot_90, self.rot_180, self.rot_270, self.reflex_vert, self.reflex_hor, self.reflex_diag, self.reflex_antidiag]
        self.interpolation_mode = 'nearest' # ce paramètre n'a pas été testé
        self.interpolation_order = 0 # testé pour 0 (nearest) 1 (bilineaire) 3 (cubique) 0 plus rapide que 1 et 3

    ## isometries
    def reduce(self,parent,child):
        ratio = float(child)/float(parent)
        return BlocJacquin(scipy.ndimage.interpolation.zoom(self.pixels, ratio,mode=self.interpolation_mode,order=self.interpolation_order))
                
    def identity(self):
        return self
        
    def rot_90(self):
        return BlocJacquin(np.rot90(self.pixels))
        
    def rot_180(self):
        return self.rot_90().rot_90()
    
    def rot_270(self):
        return self.rot_180().rot_90()
    
    def reflex_vert(self):
        return BlocJacquin(np.fliplr(self.pixels))
    
    def reflex_hor(self):
        return BlocJacquin(np.flipud(self.pixels))
    
    def reflex_diag(self):
        return self.reflex_vert().rot_90()
    
    def reflex_antidiag(self):
        return self.reflex_hor().rot_90()
        
    def regression_lin(self, bloc):
        #self est le bloc de départ, bloc est le bloc d'arrivé
        # on peut utiliser regression[1] maintenant que le pb de uint est reglé, mais en fait c'est plus long de manière étonnante.
        x = np.concatenate(self.pixels)#aligner la matrice
        y = np.concatenate(bloc.pixels)
        A = np.array([ x, np.ones(len(x))]) #[x, pondération = 1]

        regression = np.linalg.lstsq(A.T,y)
        coeff = regression[0] 
        return coeff[0],coeff[1]
        
    def contrast_lux(self, contraste, lux):
        return BlocJacquin(contraste*self.pixels + lux)
    

        
        



        
        
    