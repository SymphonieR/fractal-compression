# -*- coding: utf-8 -*-

from math import log10
import time 
## Import internal modules
from module_pixels import *
from module_image_jacquin import * 
from module_io import * 
from extension_rgb import *

import module_compression
import matplotlib.pyplot as plt

class evaluation:
    def __init__(self, evaluate):
        self.start = time.clock()
        self.values = []
        self.evaluate = evaluate
    
    def __len__(self):
        return len(self.evaluate)
        
    def calculate_EQM(self, np_compressed, np_original):
        original = Pixels(np_original)
        compressed = Pixels(np_compressed)
        return original.distance(compressed)/float(original.height*original.width)
    
    def calculate_PSNR(self, np_original, np_compressed):
        EQM = self.calculate_EQM(np_original, np_compressed)
        return 10*log10((255**2)/EQM)
        
    def evaluate_iteration(self, *images):
        if self.evaluate == 'time':
            end = time.clock()
            self.values.append(end - self.start)
            
        if self.evaluate == 'error':
            EQM = self.calculate_EQM(images[0], images[1])
            self.values.append(EQM)
        
        if self.evaluate == 'PSNR':
            PSNR = self.calculate_PSNR(images[0], images[1])
            self.values.append(PSNR)
    
    def draw_graph(self):
        x = np.arange(len(self.values))
        plt.plot(x, self.values)
        plt.xlabel('iterations')
        plt.ylabel(self.evaluate)
        plt.show()
    
        
def compare_graph(list,N):
    index = next(iter(list))
    critere = list[index].evaluate
    
    x = np.arange(N).tolist()
    plots = []
    
    for bloc in list:
        
        y =list[bloc].values
        pk, = plt.plot(x,y, label = bloc)
        plots.append(pk)
    
    if critere == 'time':
        plt.title('Evaluation du temps')
    if critere == 'error':
        plt.title("Evaluation de l'erreur moyenne quadratique")
    
    print(plots)
    plt.legend(handles = plots)
    plt.xlabel('iterations')
    plt.ylabel(critere)
    plt.show()

def ratio(image, ifs):
    dimensions = np.shape(image)
    bits_image = 8*dimensions[0]*dimensions[1]
    bits_ifs = len(ifs)*15 + 12
    return bits_image/bits_ifs

def calculate_times(dico):
    return sum(dico.values())
    
def apply(target, attractor, RGB, N, sizes, evaluate, save, savename):
    
    resultats = {}
    
    ###Criteres d'evaluation
    errors = {}
    PSNR_blocs = {}
    compression_times = {}
    decompression_times = {}
    compression_ratio = {}
    
    IFS = {}
    decompressed = {}
    
    for size in sizes:

        source_size = size[0] # Taille bloc source
        destination_size = size[1] # Taille bloc destination
    
        index = str(source_size) + 'x' + str(destination_size)
        savename += index
        
        ## Rapport des paramètres à l'utilisateur (à completer)
        print('********')
        print('Blocs : '+str(source_size)+'x'+str(destination_size))
        print('********')
    
        if RGB:
            
            ### Compression
            print('Compression (RGB)...')
            start_compression = time.clock()
            ifs = module_compression.compress_rgb(target, source_size, destination_size)
            end_compression = time.clock()
            print('Done.')
            
            ###Decompression
            print('Decompression (RGB)...')
            start_decompression = time.clock()
            rebuilt = module_compression.decompress_rgb(source_size, destination_size, ifs, attractor, N)
            end_decompression = time.clock()
            print('Done.')
            show_image_rgb(rebuilt)
            
            ###Evaluation 
            if evaluate:
                compression_times[index]= end_compression - start_compression
                decompression_times[index] = end_decompression - start_compression
        
        
        else:
            
            ### Compression
            print('Compression (greyscale)...')
            start_compression = time.clock()
            ifs = module_compression.compress(target,source_size, destination_size)
            end_compression = time.clock()
            print('Done.')
            
            ### Decompression
            print('Decompression (greyscale)...')
            if not evaluate:
                if save:
                    rebuilt = module_compression.decompress(source_size, destination_size, ifs, attractor, N, savename)
                else:
                    rebuilt = module_compression.decompress(source_size, destination_size, ifs, attractor, N)
            ###Evaluation
            else:
                ###Sauvegarde eventuelle
                if save:
                    #erreur
                    rebuilt, EQM = module_compression.decompress(source_size, destination_size, ifs, attractor, N, savename, 'error', target)    
                    rebuilt, PSNR = module_compression.decompress(source_size, destination_size, ifs, attractor, N, False, 'PSNR', target) 
                    #temps de décompression
                    rebuilt, times = module_compression.decompress(source_size, destination_size, ifs, attractor, N, False, 'time') 
                    
                else:
                    #erreur 
                    rebuilt, EQM = module_compression.decompress(source_size, destination_size, ifs, attractor, N, False, 'error', target) 
                    rebuilt, PSNR = module_compression.decompress(source_size, destination_size, ifs, attractor, N, False, 'PSNR', target) 
                    #temps de décompression
                    #temps de décompression 
                    rebuilt, times = module_compression.decompress(source_size, destination_size, ifs, attractor, N, False, 'time') 
                    
                ###stockage des mesures
                compression_times[index]= end_compression - start_compression
                errors[index] = EQM
                decompression_times[index] = times.values.pop()
                compression_ratio[index] = ratio(target, ifs)
                PSNR_blocs[index] = PSNR
        IFS[index] = ifs
        decompressed[index] = rebuilt

    resultats = {'IFS': IFS, 'images': decompressed, 'errors': errors, 'PSNR': PSNR_blocs, 'duree_compression': compression_times, 'duree_decompression': decompression_times, 'taux': compression_ratio}
    
    return resultats
