# -*- coding: utf-8 -*-
##Métaclasse pour les autres classes d'images

import numpy as np

class Pixels:

    def __init__(self,data):
        """
        Data = donnée d'entrée pour une matrice de pixels.
        Format : numpy
        """
        self.pixels = data
        dimensions = np.shape(self.pixels)
        self.height = dimensions[0]
        self.width = dimensions[1]


    def set_p(self,x,y,value):
        self.pixels[x][y] = value
    
    def get_p(self,x,y):
        return self.pixels[x][y]
    
    def __sub__(self, other):
        """
            Override l'opérateur -
        """
        return Pixels(self.pixels-other.pixels)

    def get_pixels(self):
         return self.pixels
         
    def to_long(self):
        self.pixels = np.int64(self.pixels)
    
    def to_u8(self):
        self.pixels = np.uint8(self.pixels)
    
    def to_float(self):
        self.pixels = np.float64(self.pixels)
        
    def distance(self, bloc):
        # Norme 2 au carré
        error_quadra = (self.pixels.astype('float') - bloc.pixels.astype('float'))**2
        return np.sum(error_quadra)
    